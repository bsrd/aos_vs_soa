require 'benchmark'

Vector = Struct.new(:x, :y)

Vectors = Struct.new(:x, :y) do
  def initialize(*)
    super
    self.x ||= []
    self.y ||= []
  end
end

array_of_vectors = []
struct_of_arrays = Vectors.new

(0..999999).each do |n|
  array_of_vectors[n] = Vector.new(n, n)
  struct_of_arrays.x[n] = n
  struct_of_arrays.y[n] = n
end

Benchmark.bm do |x|
  x.report('AOS:') do
    array_of_vectors.inject(Vector.new(0, 0)) do |acc, v|
      acc.x += v.x
      acc.y += v.y
      acc
    end
  end

  x.report('SOA:') do
    struct_of_arrays.x.inject(0) { |acc, n| acc + n }
    struct_of_arrays.y.inject(0) { |acc, n| acc + n }
  end
end
