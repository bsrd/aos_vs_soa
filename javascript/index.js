const ARRAY_SIZE = 100000

class Vector {
    constructor(x, y) {
        this.x = x
        this.y = y
    }
}

class Vectors {
    constructor() {
        this.x = []
        this.y = []
    }
}

array_of_vectors = []
struct_of_arrays = new Vectors()

for (let i = 0; i < ARRAY_SIZE; i++) {
    array_of_vectors[i] = new Vector(i, i)
    struct_of_arrays.x[i] = i
    struct_of_arrays.y[i] = i
}

start = process.hrtime.bigint();
array_of_vectors.reduce((acc, v) => { acc.x += v.x; acc.y += v.y; return acc; }, new Vector(0, 0))
stop = process.hrtime.bigint();
console.log('AOS: ', stop - start)

start = process.hrtime.bigint();
struct_of_arrays.x.reduce((acc, v) => acc + v)
struct_of_arrays.y.reduce((acc, v) => acc + v)
stop = process.hrtime.bigint();
console.log('SOA: ', stop - start)
