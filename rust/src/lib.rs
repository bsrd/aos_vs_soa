#![feature(test)]
extern crate test;

const SAMPLE_SIZE: usize = 100000;

fn sum() -> u32 {
    (0 .. SAMPLE_SIZE).fold(0, |a, b| a + b) as u32
}

#[derive(Clone, Copy)]
struct Vector {
    x: u32,
    y: u32
}

struct Vectors {
    x: [u32; SAMPLE_SIZE],
    y: [u32; SAMPLE_SIZE]
}

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    #[test]
    fn vector_aos_work() {
        let mut vectors = [Vector { x: 0, y: 0 }; SAMPLE_SIZE];

        for i in 0..SAMPLE_SIZE {
            vectors[i] = Vector { x: i as u32, y: i as u32 }
        }

        let result = vectors.iter().fold(Vector { x: 0, y: 0 }, |mut acc, v| {
            acc.x += v.x;
            acc.y += v.y;
            acc
        });

        let expected_sum = sum();

        assert_eq!(expected_sum, result.x);
        assert_eq!(expected_sum, result.y);
    }

    #[bench]
    fn bench_vector_aos(b: &mut Bencher) {
        let mut vectors = [Vector { x: 0, y: 0 }; SAMPLE_SIZE];

        for i in 0..SAMPLE_SIZE {
            vectors[i] = Vector { x: i as u32, y: i as u32 }
        }

        let expected_sum = sum();

        b.iter(|| {
            let result = vectors.iter().fold(Vector { x: 0, y: 0 }, |mut acc, v| {
                acc.x += v.x;
                acc.y += v.y;
                acc
            });

            assert_eq!(expected_sum, result.x);
            assert_eq!(expected_sum, result.y);
        });
    }

    #[test]
    fn vector_soa_work() {
        let mut vectors = Vectors { x: [0; SAMPLE_SIZE], y: [0; SAMPLE_SIZE] };

        for i in 0..SAMPLE_SIZE {
            vectors.x[i] = i as u32;
            vectors.y[i] = i as u32;
        }

        let expected_sum = sum();

        assert_eq!(expected_sum, vectors.x.iter().fold(0, |mut acc, v| { acc + v }));
        assert_eq!(expected_sum, vectors.y.iter().fold(0, |mut acc, v| { acc + v }));
    }

    #[bench]
    fn bench_vector_soa(b: &mut Bencher) {
        let mut vectors = Vectors { x: [0; SAMPLE_SIZE], y: [0; SAMPLE_SIZE] };

        for i in 0..SAMPLE_SIZE {
            vectors.x[i] = i as u32;
            vectors.y[i] = i as u32;
        }

        let expected_sum = sum();

        b.iter(|| {
            let x = vectors.x.iter().fold(0, |mut acc, v| { acc + v });
            let y = vectors.y.iter().fold(0, |mut acc, v| { acc + v });

            assert_eq!(expected_sum, x);
            assert_eq!(expected_sum, y);
        });
    }
}
